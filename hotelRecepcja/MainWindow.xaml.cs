﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace hotelRecepcja
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private enum WindowModes

        {
            Reservations,
            Rooms,
            Clients
        };

        private WindowModes _mode;
        private PageClientsView _pageClientsView = null;
        private PageReservationsView _pageReservationsView = null;
        private PageRoomsView _pageRoomsView = null;
        private List<RepeatButton> ListButtonsTop = null;
        public MainWindow()
        {
            InitializeComponent();
            InitializePages();
            AssignButtons();
            _mode = WindowModes.Reservations;
            ButtonReservationsView.Background = Application.Current.TryFindResource("BrushCenter") as SolidColorBrush;
            FrameMainView.NavigationService.Navigate(_pageReservationsView);

        }

        private void CheckLogin()
        {
            if(!(new LoginWindow().ShowLogin()))
                this.Close();
        }

        private void AssignButtons()
        {
            ListButtonsTop = new List<RepeatButton> {ButtonReservationsView, ButtonRoomsView, ButtonClientsView};
        }

        private void ButtonCloseWindow_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ButtonMinimizeWindow_OnClick(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void InitializePages()
        {
            _pageReservationsView = new PageReservationsView(this);
            _pageClientsView = new PageClientsView(this);
            _pageRoomsView = new PageRoomsView(this);

        }

        private void MainWindow_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void UpdateHighlight(WindowModes mode)
        {
            ListButtonsTop[(int)_mode].Background = Application.Current.TryFindResource("BrushMenuTop") as SolidColorBrush;
            ListButtonsTop[(int)(_mode = mode)].Background = Application.Current.TryFindResource("BrushCenter") as SolidColorBrush;
            UpdateLayout();
        }

        private void ButtonReservationsView_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowModes.Reservations) return;
            UpdateHighlight(WindowModes.Reservations);
            FrameMainView.NavigationService.Navigate(_pageReservationsView);
            _pageReservationsView.Refresh();
        }

        private void ButtonRoomsView_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowModes.Rooms) return;
            UpdateHighlight(WindowModes.Rooms);
            FrameMainView.NavigationService.Navigate(_pageRoomsView);
            _pageRoomsView.Refresh();
        }

        private void ButtonClientsView_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowModes.Clients) return;
            UpdateHighlight(WindowModes.Clients);
            FrameMainView.NavigationService.Navigate(_pageClientsView);
            _pageClientsView.Refresh();
        }


    }
}
