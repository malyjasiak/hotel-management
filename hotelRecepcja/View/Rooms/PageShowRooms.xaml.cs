﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace hotelRecepcja.Rooms
{
    /// <summary>
    /// Interaction logic for PageShowRooms.xaml
    /// </summary>
    public partial class PageShowRooms : Page
    {
        private PageRoomsView _parent;
        public PageShowRooms(PageRoomsView parent, bool multirowSelection)
        {
            _parent = parent;
            InitializeComponent();
            GetData();
            DataGridShowRooms.SelectionMode = multirowSelection ? DataGridSelectionMode.Extended : DataGridSelectionMode.Single;
        }

        public void Refresh()
        {
            this.GetData();
            TextBoxCapacity.Text = TextBoxCapacity.ToolTip.ToString();
            TextBoxFloor.Text = TextBoxFloor.ToolTip.ToString();
            TextBoxNumber.Text = TextBoxNumber.ToolTip.ToString();
            TextBoxPriceTo.Text = TextBoxPriceTo.ToolTip.ToString();

        }

        public void GetData()
        {
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand("Select * from rooms_view", con))
                    {
                        con.Open();
                        DataTable dt = new DataTable();
                        cmd.Prepare();
                        dt.Load(cmd.ExecuteReader());
                        DataGridShowRooms.DataContext = null;
                        DataGridShowRooms.DataContext = dt;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        public void SelectRooms(string idReservation)
        {
            var idRoomList = new List<string>();
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand($"Select id_room from reserved_rooms where id_reservation = '{idReservation}';", con))
                    {
                        con.Open();
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            idRoomList.Add(reader[0].ToString());
                        }
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error", MessageBoxButton.OK);
            }



            foreach (var room in idRoomList)
            {
                var rows = (DataGridShowRooms.DataContext as DataTable)?.Rows;
                if (rows == null) return;
                for (var i = 0; i < rows.Count; i++)
                {
                    if (rows[i][0].ToString() == room)
                        DataGridShowRooms.SelectedItems.Add(DataGridShowRooms.Items[i]);
                }
            }
        }

        public List<string> GetSelection()
        {
            var list = new List<string>();
            foreach (var item in DataGridShowRooms.SelectedItems)
            {
                list.Add((item as DataRowView)?[0].ToString());
            }
            return list;
        }
        private void TextBoxSearch_OnGotFocus(object sender, RoutedEventArgs e)
        {
            var tb = e.Source as TextBox;
            if (tb == null) return;
            if (tb.Text.Equals(tb.ToolTip.ToString()))
                tb.Text = "";

        }

        private void TextBoxSearch_OnLostFocus(object sender, RoutedEventArgs e)
        {
            var tb = e.Source as TextBox;
            if (tb == null) return;
            if (string.IsNullOrWhiteSpace(tb.Text))
                tb.Text = tb.ToolTip.ToString();
        }

        private void Search()
        {
            var command = new StringBuilder();
            command.Append("select * from rooms_view ");

            var sbArgs = new StringBuilder();
            if (!TextBoxNumber.Text.Equals(TextBoxNumber.ToolTip))
                sbArgs.Append($"Numer_Pokoju = '{TextBoxNumber.Text}'");
            if (!TextBoxCapacity.Text.Equals(TextBoxCapacity.ToolTip))
            {
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Pojemnosc = '{TextBoxCapacity.Text}'");
            }
            if (!TextBoxFloor.Text.Equals(TextBoxFloor.ToolTip))
            {
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Pietro = '{TextBoxFloor.Text}'");
            }
            if (!TextBoxPriceTo.Text.Equals(TextBoxPriceTo.ToolTip))
            {
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Cena <= '{TextBoxPriceTo.Text}'");
            }

            if (sbArgs.Length > 0)
                command.Append($"where {sbArgs.ToString()};");


            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand(command.ToString(), con))
                    {
                        con.Open();
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());
                        DataGridShowRooms.DataContext = null;
                        DataGridShowRooms.DataContext = dt;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        private void ButtonSearch_OnClick(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void MenuItemEdit_OnClick(object sender, RoutedEventArgs e)
        {
            _parent?.RunEditMode(GetSelectedRoom());
        }

        private int GetSelectedRoom()
        {
            if (DataGridShowRooms != null)
                return int.Parse((DataGridShowRooms.SelectedItem as DataRowView)?[0].ToString());
            return -1;
        }

        private void DeleteRoom()
        {
            if (MessageBox.Show("Czy na pewno chcesz usunąć?", "Potwierdzenie", MessageBoxButton.YesNo) ==
               MessageBoxResult.No) return;
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    var row = DataGridShowRooms.SelectedItem as DataRowView;
                    using (var cmd = new MySqlCommand($"Delete from rooms where id_room={row?[0]}", con))
                    {
                        con.Open();
                        var result = cmd.ExecuteNonQuery();
                        MessageBox.Show(result == 0 ? "Operacja nie powiodła się" : "Pomyślnie usunięto pokój");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nie wolno usunąć pokoju, z którym powiązane są rezerwacje!" + Environment.NewLine +
                    "Najpierw usuń lub zmień rezerwacje");
            }
            Refresh();
        }

        private void MenuItemDelete_OnClick(object sender, RoutedEventArgs e)
        {
            if (_parent == null) return;
            DeleteRoom();
        }
    }
}
