﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MySql.Data.MySqlClient;

namespace hotelRecepcja.Rooms
{
    /// <summary>
    /// Interaction logic for PageAddRoom.xaml
    /// </summary>
    public partial class PageAddRoom : Page
    {
        private int _id;
        private enum Modes
        {
            Add,
            Edit
        };

        private Modes _mode;
        private PageRoomsView _parent;
        public PageAddRoom(PageRoomsView parent)
        {
            _parent = parent;
            InitializeComponent();
            Refresh();
        }

        public void Refresh()
        {
            TextBoxCapacity.Text = "";
            TextBoxFloor.Text = "";
            TextBoxNumber.Text = "";
            TextBoxPrice.Text = "";
            TextBoxCapacity.BorderBrush =
                TextBoxFloor.BorderBrush =
                    TextBoxNumber.BorderBrush =
                        TextBoxPrice.BorderBrush =
                           DataGridStandards.BorderBrush = new SolidColorBrush(Colors.Gray);
            GetData();
        }

        public void RunAddMode()
        {
            _mode = Modes.Add;
            Refresh();
        }

        public void RunEditMode(int id)
        {
            _mode = Modes.Edit;
            _id = id;
            Refresh();
            FillForm(id);
        }

        private void FillForm(int id)
        {
            using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
            {
                using (
                    var cmd =
                        new MySqlCommand($"select * from rooms where id_room={id}", con))
                {
                    con.Open();
                    var reader = cmd.ExecuteReader();
                    if (!reader.Read()) return;
                    TextBoxCapacity.Text = reader["CAPACITY"].ToString();
                    TextBoxFloor.Text = reader["FLOOR"].ToString();
                    TextBoxNumber.Text = reader["ROOM_NUMBER"].ToString();
                    TextBoxPrice.Text = reader["PRICE"].ToString();
                    var standardId = reader["ID_STANDARD"].ToString();
                    SelectStandard(standardId);
                }
            }
        }

        private void SelectStandard(string id)
        {
            var rows = (DataGridStandards.DataContext as DataTable)?.Rows;
            if (rows == null) return;
            for (var i = 0; i < rows.Count; i++)
            {
                if ( rows[i][0].ToString() == id) DataGridStandards.SelectedIndex = i;
            }
        }
        private void GetData()
        {
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (
                        var cmd =
                            new MySqlCommand(
                                "select id_standard as 'ID Standardu', standard_level as 'Opis' from standards", con))
                    {
                        con.Open();
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());
                        DataGridStandards.DataContext = null;
                        DataGridStandards.DataContext = dt;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        private string GetSelectedStandard()
        {
            return (DataGridStandards.SelectedItem as DataRowView)?[0].ToString();
        }

        private void AddRoom()
        {

            if (!Validate()) return;
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand("insert into rooms (room_number, capacity, price, id_standard, floor) " +
                                                      $"values ('{TextBoxNumber.Text}','{TextBoxCapacity.Text}','{TextBoxPrice.Text}','{GetSelectedStandard()}','{TextBoxFloor.Text}')", con))
                    {
                        con.Open();
                        int response = cmd.ExecuteNonQuery();
                        MessageBox.Show((response == 0) ? "Operacja nie powiodła się" : "Pomyślnie dodano pokój");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        private void EditRoom()
        {
           if(!Validate()) return;
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand("update rooms " +
                                                      $"set room_number={TextBoxNumber.Text}, capacity={TextBoxCapacity.Text}, price={TextBoxPrice.Text}, id_standard={GetSelectedStandard()}, floor={TextBoxFloor.Text} "+
                                                      $"where id_room={_id};"
                                                      , con))
                    {
                        con.Open();
                        int response = cmd.ExecuteNonQuery();
                        MessageBox.Show((response == 0) ? "Operacja nie powiodła się" : "Pomyślnie dodano pokój");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        private bool Validate()
        {
            var validationOk = ValidateNumericTextBox(TextBoxCapacity);
            if (!ValidateNumericTextBox(TextBoxPrice)) validationOk = false;
            if (!ValidateNumericTextBox(TextBoxFloor)) validationOk = false;
            if (!ValidateNumericTextBox(TextBoxNumber)) validationOk = false;
            if (DataGridStandards.SelectedIndex == -1)
            {
                DataGridStandards.BorderBrush = new SolidColorBrush(Colors.Red);
                validationOk = false;
            }
            return validationOk;
        }

        private bool ValidateNumericTextBox(TextBox tb)
        { 
            if (!int.TryParse(tb.Text, out int n) || string.IsNullOrWhiteSpace(tb.Text))
            {
                tb.BorderBrush = new SolidColorBrush(Colors.Red);
                return false;
            }
            tb.BorderBrush = new SolidColorBrush(Colors.Gray);
            return true;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            switch (_mode)
            {
                case Modes.Add:
                    AddRoom();
                    break;
                case Modes.Edit:
                    EditRoom();
                    break;
            }
        }


        private void NumericalValidation(object sender, RoutedEventArgs e)
        {
            ValidateNumericTextBox(sender as TextBox);
        }

    }
}
