﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using hotelRecepcja.Reservations;
using hotelRecepcja.View.Reservations;

namespace hotelRecepcja
{
    /// <summary>
    /// Interaction logic for PageReservationsView.xaml
    /// </summary>
    public partial class PageReservationsView : Page
    {
        private enum WindowMode { Show, Add, Confirm, Current }

        private WindowMode _mode;
        public MainWindow Parent;
        private PageShowReservations _pageShowReservations;
        private PageAddReservation _pageAddReservation;
        private PageReservationsToConfirm _pageReservationsToConfirm;
        private PageCurrentReservations _pageCurrentReservations;

        private List<RepeatButton> _listButtonsLeft;
        public PageReservationsView(MainWindow parent)
        {
            Parent = parent;
            InitializeComponent();
            InitializePages();
            AssignButtons();
            _mode = WindowMode.Show;
            ButtonShowReservations.Background = Application.Current.TryFindResource("BrushLeftMenuActive") as SolidColorBrush;
            FrameMainView.Navigate(_pageShowReservations);
        }


        private void InitializePages()
        { 
            _pageShowReservations = new PageShowReservations(this);
            _pageAddReservation = new PageAddReservation(this);
            _pageReservationsToConfirm = new PageReservationsToConfirm(this);
            _pageCurrentReservations = new PageCurrentReservations(this);
        }

        private void AssignButtons()
        {
            _listButtonsLeft = new List<RepeatButton>()
            {
                ButtonShowReservations,
                ButtonAddReservation,
                ButtonShowReservationsToConfirm,
                ButtonShowCurrentReservations
            };

        }

        public void Refresh()
        {
            UpdateLayout();
        }

        public void RunEditMode(string id)
        {
            UpdateHighlight(WindowMode.Add);
            _pageAddReservation.Refresh();
            _pageAddReservation.RunEditMode(id);
            FrameMainView.Navigate(_pageAddReservation);
        }

        private void UpdateHighlight(WindowMode mode)
        {
            _listButtonsLeft[(int)_mode].Background = new SolidColorBrush(Colors.Transparent);
            _listButtonsLeft[(int)(_mode = mode)].Background = Application.Current.TryFindResource("BrushLeftMenuActive") as SolidColorBrush;
        }
        private void ButtonShowReservations_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowMode.Show) return;
            _pageShowReservations.Refresh();
            FrameMainView.Navigate(_pageShowReservations);
            UpdateHighlight(WindowMode.Show);
        }

        private void ButtonShowReservationsToConfirm_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowMode.Confirm) return;
            _pageReservationsToConfirm.Refresh();
            FrameMainView.Navigate(_pageReservationsToConfirm);
            UpdateHighlight(WindowMode.Confirm);
        }

        private void ButtonAddReservation_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowMode.Add) return;
            _pageAddReservation.RunAddMode();
            FrameMainView.Navigate(_pageAddReservation);
            UpdateHighlight(WindowMode.Add);
        }

        private void ButtonShowCurrentReservations_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowMode.Current) return;
            _pageCurrentReservations.Refresh();
            FrameMainView.Navigate(_pageCurrentReservations);
            UpdateHighlight(WindowMode.Current);
        }
    }
}
